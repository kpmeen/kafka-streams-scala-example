#!/bin/bash

docker exec -it kafka1 kafka-topics \
  --bootstrap-server kafka1:9092 \
  --topic wordcount-input-avro \
  --create \
  --partitions 3 \
  --replication-factor 1 \
  --config cleanup.policy=delete \
  --config retention.ms=1800000

docker exec -it kafka1 kafka-topics \
  --bootstrap-server kafka1:9092 \
  --topic wordcount-output-avro \
  --create \
  --partitions 3 \
  --replication-factor 1 \
  --config cleanup.policy=compact \
  --config retention.ms=1800000