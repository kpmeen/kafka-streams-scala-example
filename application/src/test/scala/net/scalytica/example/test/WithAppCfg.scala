package net.scalytica.example.test

import net.scalytica.example.streams.Configuration

trait WithAppCfg {

  implicit protected lazy val appCfg: Configuration.AppCfg =
    Configuration.loadURI(getClass.getResource("/application-test.conf").toURI)

  implicit protected lazy val kafkaStreamsProps: Map[String, AnyRef] =
    appCfg.kafkaStreamsConfig

}
